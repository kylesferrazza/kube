#!/bin/sh

if [ -z "$ANSIBLE_BECOME_PASS" ]; then
  echo "You need to set ANSIBLE_BECOME_PASS."
  exit 1
fi

ansible-playbook -i inventory.cfg reset.yml -e "ansible_become_password='{{ lookup('env', 'ANSIBLE_BECOME_PASS') }}'"
