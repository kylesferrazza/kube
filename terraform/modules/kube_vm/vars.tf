variable "vc_address" {
  type = string
}

variable "vc_username" {
  type = string
}

variable "vc_password" {
  type = string
}

variable "vc_datacenter" {
  type = string
}

variable "vc_network" {
  type = string
}

variable "vc_datastore" {
  type = string
}

variable "esxi_host" {
  type = string
}

variable "vm_name" {
  type = string
}

variable "vm_ip" {
  type = string
}

variable "vc_folder" {
  type = string
}
