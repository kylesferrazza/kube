[router]
kube-router ansible_host=${router_ip}

[router:vars]
ansible_user=vyos
ansible_connection=ansible.netcommon.network_cli
ansible_network_os=vyos.vyos.vyos
ansible_become=yes
ansible_become_method=enable

[primary]
kube-primary ansible_host=${primary_ip}

[workers]
%{ for index, ip in worker_ips ~}
kube-worker-0${index+1} ansible_host=${ip}
%{ endfor ~}
