# kube

```
pushd terraform
terraform apply
popd

pushd ansible
./provision.sh
popd
```

On primary:
```
sudo kubeadm init --pod-network-cidr=10.100.0.0/16 --apiserver-cert-extra-sans kube.kylesferrazza.com
mkdir -p $HOME/.kube
sudo cp -f /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Locally:
```
rsync -avP 192.168.110.100:.kube/config ~/.kube/config
kubectl create -f kubernetes-config/networking/calico.yaml

# Wait for pods to start
watch kubectl get pods -n kube-system

# primary should show Ready
kubectl get no
```

Run the join command on other nodes:
```
ansible -i ansible/inventory.cfg workers -m shell -a 'sudo ...'
```

Locally:
```
# Wait for pods to start
watch kubectl get pods -n kube-system

# all nodes should show Ready
kubectl get no

./assign-labels.sh

# all worker nodes should show worker label
kubectl get no
```
