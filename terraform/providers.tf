provider "vsphere" {
  user = var.vc_username
  password = var.vc_password
  vsphere_server = var.vc_address
  allow_unverified_ssl = true
}
