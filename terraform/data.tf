data "vsphere_datacenter" "dc" {
  name = var.vc_datacenter
}

data "vsphere_network" "network" {
  name = var.vc_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "mgmt_network" {
  name = var.vc_mgmt_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "public_network" {
  name = var.vc_public_network
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name = var.vc_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name = "${var.esxi_host}/Resources"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "vyos_template" {
  name = "vyos-templ"
  datacenter_id = data.vsphere_datacenter.dc.id
}
