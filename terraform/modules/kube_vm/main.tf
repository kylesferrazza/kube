resource "vsphere_virtual_machine" "vm" {
  name = var.vm_name
  folder = var.vc_folder
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id = data.vsphere_datastore.datastore.id

  num_cpus = 2
  memory = 8192
  guest_id = data.vsphere_virtual_machine.ubuntu_20_template.guest_id

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout = 2

  enable_disk_uuid = true

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.ubuntu_20_template.network_interface_types[0]
  }

  disk {
    label = "disk0"
    size = data.vsphere_virtual_machine.ubuntu_20_template.disks.0.size
    eagerly_scrub = data.vsphere_virtual_machine.ubuntu_20_template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.ubuntu_20_template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.ubuntu_20_template.id
    customize {
      linux_options {
        host_name = var.vm_name
        domain = "local"
      }
      network_interface {
        ipv4_address = var.vm_ip
        ipv4_netmask = 24
      }
      ipv4_gateway = "192.168.110.1"
      dns_server_list = ["192.168.110.1"]
    }
  }
}

output "default_ip_address" {
  value = vsphere_virtual_machine.vm.default_ip_address
}
