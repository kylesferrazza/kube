variable "vc_address" {
  type = string
}

variable "vc_username" {
  type = string
}

variable "vc_password" {
  type = string
}

variable "vc_datacenter" {
  type = string
}

variable "vc_network" {
  type = string
}

variable "vc_public_network" {
  type = string
}

variable "vc_mgmt_network" {
  type = string
}

variable "vc_datastore" {
  type = string
}

variable "esxi_host" {
  type = string
}

variable "vc_datastore_2" {
  type = string
}

variable "esxi_host_2" {
  type = string
}

variable "worker_count" {
  type = string
  default = 4
}
