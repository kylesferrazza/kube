#!/bin/sh
NODES=$(kubectl get nodes -o json | jq -r '.items[].metadata.name' | grep worker)

for NODE in $NODES; do
  kubectl label node $NODE node-role.kubernetes.io/worker=worker
done
