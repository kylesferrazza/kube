{
  description = "bare-metal ESXi kubernetes configuration";

  outputs = { self, nixpkgs }: 
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs {
      inherit system;
    };
  in {
    devShell.x86_64-linux = import ./shell.nix {
      inherit pkgs;
    };
  };
}
