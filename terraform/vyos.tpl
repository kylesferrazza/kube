delete interfaces ethernet eth2 address dhcp

set interfaces ethernet eth0 description lab
set interfaces ethernet eth0 address dhcp

set interfaces ethernet eth1 description 44net
set interfaces ethernet eth1 address dhcp

set interfaces ethernet eth2 description kube
set interfaces ethernet eth2 address 192.168.110.1/24
set service router-advert interface eth2 prefix 2001:470:89cd:8000::/64

set protocols static route 0.0.0.0/0 next-hop 44.44.127.241

set service dns forwarding allow-from 192.168.110.0/24
set service dns forwarding listen-address 192.168.110.1
set service dns forwarding name-server 172.24.0.1

set service ssh port 22
set service ssh disable-password-authentication

set system host-name kube-router
set system name-server 172.24.0.1

set system static-host-mapping host-name kube-primary inet ${primary_ip}

%{ for index, ip in worker_ips ~}
set system static-host-mapping host-name kube-worker-0${index+1} inet ${ip}
%{ endfor ~}


set nat source rule 1 outbound-interface eth1
set nat source rule 1 source address 192.168.110.0/24
set nat source rule 1 translation address masquerade

set nat source rule 2 outbound-interface eth0
set nat source rule 2 source address 192.168.110.0/24
set nat source rule 2 translation address masquerade

set firewall name OUTSIDE-IN default-action drop
set firewall name OUTSIDE-IN rule 1 action accept
set firewall name OUTSIDE-IN rule 1 state established enable
set firewall name OUTSIDE-IN rule 1 state related enable

set firewall ipv6-name OUTSIDE6-IN default-action drop
set firewall ipv6-name OUTSIDE6-IN rule 1 action accept
set firewall ipv6-name OUTSIDE6-IN rule 1 state established enable
set firewall ipv6-name OUTSIDE6-IN rule 1 state related enable

set interfaces ethernet eth1 firewall in name OUTSIDE-IN
set interfaces ethernet eth1 firewall in ipv6-name OUTSIDE6-IN

set firewall name OUTSIDE-LOCAL default-action drop
set firewall name OUTSIDE-LOCAL rule 1 action accept
set firewall name OUTSIDE-LOCAL rule 1 state established enable
set firewall name OUTSIDE-LOCAL rule 1 state related enable

set firewall ipv6-name OUTSIDE6-LOCAL default-action drop
set firewall ipv6-name OUTSIDE6-LOCAL rule 1 action accept
set firewall ipv6-name OUTSIDE6-LOCAL rule 1 state established enable
set firewall ipv6-name OUTSIDE6-LOCAL rule 1 state related enable

set firewall name OUTSIDE-LOCAL rule 10 action accept
set firewall name OUTSIDE-LOCAL rule 10 icmp type-name echo-request
set firewall name OUTSIDE-LOCAL rule 10 protocol icmp
set firewall name OUTSIDE-LOCAL rule 10 state new enable

set firewall ipv6-name OUTSIDE6-LOCAL rule 10 action accept
set firewall ipv6-name OUTSIDE6-LOCAL rule 10 protocol icmpv6

set firewall ipv6-name OUTSIDE6-LOCAL rule 11 description dhcpv6
set firewall ipv6-name OUTSIDE6-LOCAL rule 11 action accept
set firewall ipv6-name OUTSIDE6-LOCAL rule 11 protocol udp
set firewall ipv6-name OUTSIDE6-LOCAL rule 11 destination port 546

set interfaces ethernet eth1 firewall local name OUTSIDE-LOCAL
set interfaces ethernet eth1 firewall local ipv6-name OUTSIDE6-LOCAL

set nat destination rule 11 destination port 6443
set nat destination rule 11 inbound-interface eth1
set nat destination rule 11 protocol tcp
set nat destination rule 11 translation address ${primary_ip}

set firewall name OUTSIDE-IN rule 11 action accept
set firewall name OUTSIDE-IN rule 11 destination address ${primary_ip}
set firewall name OUTSIDE-IN rule 11 destination port 6443
set firewall name OUTSIDE-IN rule 11 protocol tcp
set firewall name OUTSIDE-IN rule 11 state new enable


set nat destination rule 12 destination port 80
set nat destination rule 12 inbound-interface eth1
set nat destination rule 12 protocol tcp
set nat destination rule 12 translation address ${primary_ip}
set nat destination rule 12 translation port 30001

set firewall name OUTSIDE-IN rule 12 action accept
set firewall name OUTSIDE-IN rule 12 destination address ${primary_ip}
set firewall name OUTSIDE-IN rule 12 destination port 30001
set firewall name OUTSIDE-IN rule 12 protocol tcp
set firewall name OUTSIDE-IN rule 12 state new enable


set nat destination rule 13 destination port 443
set nat destination rule 13 inbound-interface eth1
set nat destination rule 13 protocol tcp
set nat destination rule 13 translation address ${primary_ip}
set nat destination rule 13 translation port 30002

set firewall name OUTSIDE-IN rule 13 action accept
set firewall name OUTSIDE-IN rule 13 destination address ${primary_ip}
set firewall name OUTSIDE-IN rule 13 destination port 30002
set firewall name OUTSIDE-IN rule 13 protocol tcp
set firewall name OUTSIDE-IN rule 13 state new enable
