{ pkgs ? import <nixpkgs> {} }:
pkgs.mkShell {
  name = "kube";
  buildInputs = with pkgs; [
    ansible
    terraform_0_13
    kubectl
    govc
    envsubst
    jq
  ];
}
