terraform {
  backend "remote" {
    organization = "nuccdc"

    workspaces {
      name = "kyle-kubernetes"
    }
  }
}

module "kube-primary" {
  source = "./modules/kube_vm"
  vc_folder = "kyle/kube"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore
  esxi_host = var.esxi_host
  vm_name = "kube-primary"
  vm_ip = "192.168.110.100"
}

module "kube-workers-host-1" {
  count = var.worker_count
  source = "./modules/kube_vm"
  vc_folder = "kyle/kube"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore
  esxi_host = var.esxi_host
  vm_name = format("kube-worker-%02s", count.index + 1)
  vm_ip = format("192.168.110.1%02s", count.index + 1)
}

module "kube-workers-host-2" {
  count = var.worker_count
  source = "./modules/kube_vm"
  vc_folder = "kyle/kube"
  vc_address = var.vc_address
  vc_username = var.vc_username
  vc_password = var.vc_password
  vc_datacenter = var.vc_datacenter
  vc_network = var.vc_network
  vc_datastore = var.vc_datastore_2
  esxi_host = var.esxi_host_2
  vm_name = format("kube-worker-%02s", count.index + 1 + var.worker_count)
  vm_ip = format("192.168.110.1%02s", count.index + 1 + var.worker_count)
}

resource "vsphere_virtual_machine" "vyos" {
  name = "kube-router"
  folder = "kyle/kube"
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id = data.vsphere_datastore.datastore.id

  num_cpus = 2
  memory = 2048
  guest_id = data.vsphere_virtual_machine.vyos_template.guest_id

  wait_for_guest_net_timeout = 0
  wait_for_guest_ip_timeout = 2

  network_interface {
    network_id   = data.vsphere_network.mgmt_network.id
    adapter_type = data.vsphere_virtual_machine.vyos_template.network_interface_types[0]
  }

  network_interface {
    network_id   = data.vsphere_network.public_network.id
    adapter_type = data.vsphere_virtual_machine.vyos_template.network_interface_types[0]
  }

  network_interface {
    network_id   = data.vsphere_network.network.id
    adapter_type = data.vsphere_virtual_machine.vyos_template.network_interface_types[0]
  }

  disk {
    label = "disk0"
    size = data.vsphere_virtual_machine.vyos_template.disks.0.size
    eagerly_scrub = data.vsphere_virtual_machine.vyos_template.disks.0.eagerly_scrub
    thin_provisioned = data.vsphere_virtual_machine.vyos_template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.vyos_template.id
  }
}

# generate inventory file for Ansible
resource "local_file" "inventory" {
  content = templatefile("inventory.tpl",
    {
      router_ip = vsphere_virtual_machine.vyos.guest_ip_addresses[0]
      primary_ip = module.kube-primary.default_ip_address
      worker_ips = concat(module.kube-workers-host-1.*.default_ip_address, module.kube-workers-host-2.*.default_ip_address)
    }
  )
  filename = "../ansible/inventory.cfg"
}

# generate firewall config
resource "local_file" "vyos_cfg" {
  content = templatefile("vyos.tpl",
    {
      primary_ip = module.kube-primary.default_ip_address
      worker_ips = concat(module.kube-workers-host-1.*.default_ip_address, module.kube-workers-host-2.*.default_ip_address)
    }
  )
  filename = "../ansible/firewall/vyos.cfg"
}
